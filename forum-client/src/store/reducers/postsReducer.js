import {FETCH_POST_SUCCESS, FETCH_POSTS_SUCCESS, LOADING_POSTS} from "../actions/postsActions";


const initialState = {
  posts: [],
  post: {},
  loading: true
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts};
    case FETCH_POST_SUCCESS:
      return {...state, post: action.post};
    case LOADING_POSTS:
      return {...state, loading: action.cancel};
    default:
      return state;
  }
};

export default postsReducer;