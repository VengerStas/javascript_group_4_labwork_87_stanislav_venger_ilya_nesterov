import axios from "../../axiosBase";

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const LOADING_POSTS = 'LOADING_POSTS';

const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, post});
const loadingPosts = cancel => ({type: LOADING_POSTS, cancel});

export const CREATE_POSTS_SUCCESS = 'CREATE_POSTS_SUCCESS';

const createPostsSuccess = () => ({type: CREATE_POSTS_SUCCESS});

export const fetchPosts = () => {
  return dispatch => {
    return axios.get('/posts').then(
      response => dispatch(fetchPostsSuccess(response.data))
    ).finally(
      () => dispatch(loadingPosts(false))
    )
  }
};

export const createPost = postData => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    return axios.post('/posts', postData, {headers: {'Authorization': token}}).then(
      () => dispatch(createPostsSuccess())
    );
  };
};

export const fetchPost = postId => {
  return (dispatch) => {
    return axios.get('/posts/' + postId).then(response => {
      dispatch(fetchPostSuccess(response.data));
    })
  }
};