import axios from "../../axiosBase";

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});

export const fetchGetComments = postId => {
    return (dispatch) => {
        return axios.get('/comments/' + postId).then(response => {
            dispatch(fetchCommentsSuccess(response.data));
        })
    }
};

export const fetchAddComment = data => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const postId = data.post;
        return axios.post('/comments', data, {headers: {'Authorization': token}}).then(() => {
            dispatch(fetchGetComments(postId));
        })
    }
};