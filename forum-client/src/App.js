import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";
import {NotificationContainer} from 'react-notifications';
import {connect} from "react-redux";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Posts from "./containers/Posts/Posts";
import {logoutUser} from "./store/actions/userAction";
import NewPost from "./containers/NewPost/NewPost";
import PostInfo from "./containers/PostInfo/PostInfo";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <NotificationContainer/>
          <Toolbar
            user={this.props.user}
            logout={this.props.logoutUser}
          />
        </header>
        <Container style={{marginTop: '20px'}}>
          <Switch>
            <Route path="/" exact component={Posts}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/new-post" exact component={NewPost}/>
            <Route path="/post/:id" exact component={PostInfo}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
