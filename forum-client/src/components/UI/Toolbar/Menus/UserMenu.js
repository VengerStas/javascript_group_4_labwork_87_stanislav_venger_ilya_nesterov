import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';

const UserMenu = ({user, logout}) => (
  <UncontrolledDropdown nav inNavbar>
      <DropdownToggle nav caret>
          Hello, {user.username}
      </DropdownToggle>
      <DropdownMenu right>
          <DropdownItem>
              <NavLink tag={RouterNavLink} to="/new-post">New post</NavLink>
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem onClick={logout}>
              Log out
          </DropdownItem>
      </DropdownMenu>
  </UncontrolledDropdown>
);

export default UserMenu;