import React from 'react';

import imageForum from '../../assets/images/forum-image.png';
import {apiURL} from "../../constants";

import './PostThumbnail.css';

const PostThumbnail = props => {
    let image = imageForum;

    if (props.image) {
        image = apiURL + '/uploads/' + props.image;
    }

    return <img src={image} className="img-thumbnail" alt="Post" />;
};

export default PostThumbnail;
