import React, {Component} from 'react';
import {Button, Form} from "reactstrap";
import {connect} from "react-redux";
import FormElement from "../UI/Form/FormElement";
import {fetchAddComment} from "../../store/actions/commentsActions";

class CommentForm extends Component {
    state = {
        post: this.props.post,
        description: '',
    };

    submitCommentHandler = event => {
        event.preventDefault();
        const data = {
            post: this.props.post,
            description: this.state.description
        };
        this.props.addComment(data);
    };

    changeValue = event => {
        this.setState ({
            [event.target.name]: event.target.value
        });
    };


    render() {
        return (
            <div className="comment-form-block">
                <Form onSubmit={this.submitCommentHandler}>
                    <FormElement propertyName="description" title="Description" type="text" value={this.state.description} onChange={this.changeValue}/>
                    <Button onClick={(event) => this.submitCommentHandler(event)}>Add comment</Button>
                </Form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addComment: data => dispatch(fetchAddComment(data))
});

export default connect(null, mapDispatchToProps)(CommentForm);