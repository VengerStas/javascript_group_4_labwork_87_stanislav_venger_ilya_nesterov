import React, {Component} from 'react';
import {Button, Col, Container, Form, FormGroup, Input} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import Label from "reactstrap/es/Label";

class AddPostForm extends Component {
    state = {
        title: '',
        description: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <div className="add-post-block">
                <Container>
                    <Form onSubmit={this.submitFormHandler}>
                        <FormElement propertyName="title" title="Title" type="text" value={this.state.title} onChange={this.inputChangeHandler} placeholder="Enter a title of your post"/>
                        <FormElement propertyName="description" title="Description" type="textarea" value={this.state.description} onChange={this.inputChangeHandler} placeholder="Enter post description"/>
                        <FormGroup row>
                            <Label sm={2} for="image">Post Image</Label>
                            <Col sm={10}>
                                <Input
                                    type="file"
                                    name="image" id="image"
                                    onChange={this.fileChangeHandler}
                                />
                            </Col>
                        </FormGroup>
                        <Button type="submit">Create post</Button>
                    </Form>
                </Container>
            </div>
        );
    }
}

export default AddPostForm;