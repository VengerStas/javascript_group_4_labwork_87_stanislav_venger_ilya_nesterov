import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchPost} from "../../store/actions/postsActions";
import {Container} from "reactstrap";
import PostThumbnail from "../../components/PostThumbnail/PostThumbnail";
import {fetchGetComments} from "../../store/actions/commentsActions";
import CommentForm from "../../components/CommnetForm/CommentForm";


import './PostInfo.css';

class PostInfo extends Component {
    componentDidMount() {
        this.props.loadPost(this.props.match.params.id);
        this.props.loadComment(this.props.match.params.id);
    };


    render() {
        let comment = this.props.comment.map(comment => {
            return (
                <div className="comment-item" key={comment._id}>
                    <p>{comment.datetime} by <strong>{comment.user.username}</strong></p>
                    <p><strong>Description: </strong>{comment.description}</p>
                </div>
            )
        });
        return (
            <div className="post-block">
                <Container>
                    <div className="info-block">
                        <PostThumbnail image={this.props.post.image}/>
                        <h2>{this.props.post.title}</h2>
                        <p>{this.props.post.description}</p>
                    </div>
                    {
                        this.props.users ? <div className="add-comment-form-block">
                            <h4>Add your comment here:</h4>
                            <CommentForm
                                post={this.props.match.params.id}
                            />
                        </div> : null
                    }
                    <div className="comments-block">
                        {comment}
                    </div>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    users: state.users.user,
    post: state.posts.post,
    comment: state.comments.comments,
});

const mapDispatchToProps = dispatch => ({
    loadPost: (itemId) => dispatch(fetchPost(itemId)),
    loadComment: (postId) => dispatch(fetchGetComments(postId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PostInfo);