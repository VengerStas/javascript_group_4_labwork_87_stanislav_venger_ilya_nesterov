import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavLink as RouterNavLink} from "react-router-dom";
import {Card, CardText, CardTitle, Col, NavLink, Row} from "reactstrap";

import {fetchPosts} from "../../store/actions/postsActions";
import PostThumbnail from "../../components/PostThumbnail/PostThumbnail";

import './Posts.css';
import Spinner from "../../components/UI/Spinner/Spinner";


class Posts extends Component {

  componentDidMount() {
    this.props.fetchPosts()
  }

  render() {
    if (this.props.loading) {
      return <Spinner />;
    }

    const posts = this.props.posts.map(post => {
      return (
          <Col sm="6" key={post._id}>
            <Card body outline color="success" className="post-card">
              <Row>
                <Col xs="3"><PostThumbnail image={post.image}/></Col>
                <Col xs="9">
                  <CardTitle>
                    <span>{post.datetime} by </span>
                    <strong>{post.user.username}</strong>
                  </CardTitle>
                  <CardText>
                    <NavLink tag={RouterNavLink}  to={'/post/' + post._id}>{post.title}...</NavLink>
                  </CardText>
                </Col>
              </Row>
            </Card>
          </Col>
      )
    });
    return (
      <Row>
        {posts}
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts,
  loading: state.posts.loading
});

const mapDispatchToProps = dispatch => ({
  fetchPosts: () => dispatch(fetchPosts())
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);