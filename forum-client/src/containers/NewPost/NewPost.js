import React, {Component} from 'react';
import {Container} from "reactstrap";
import AddPostForm from "../../components/AddPostForm/AddPostForm";
import {connect} from "react-redux";
import {createPost} from "../../store/actions/postsActions";
import {Redirect} from "react-router-dom";

class NewPost extends Component {
    createPost = postData => {
        this.props.postCreate(postData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        if (!this.props.user) return <Redirect to="/" />;
        return (
            <div className="add-post-block">
                <Container>
                    <h4>Add new post</h4>
                </Container>
                <AddPostForm
                    onSubmit={this.createPost}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
   user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    postCreate: postData => dispatch(createPost(postData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);