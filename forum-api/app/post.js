const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const Post = require('../models/Post');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', auth, upload.single('image'), (req, res) => {
  const postData = {...req.body, user: req.user._id, datetime: new Date().toISOString()};

  if (req.file) {
    postData.image = req.file.filename;
  }

  const post = new Post(postData);

  if (req.body.description !== '' || req.body.image !== '') {
    post.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
  } else {
    return res.sendStatus(400);
  }
});

router.get('/', (req, res) => {
  Post.find().populate('user', '_id, username').sort({datetime: -1})
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error))
});

router.get('/:id', (req, res) => {
  Post.findById(req.params.id).populate('user', '_id, username')
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error))
});


module.exports = router;
