const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Post = require('./models/Post');
const Comment = require('./models/Comment');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {username: "Ilya", password: "123", token: nanoid()},
    {username: "Stas", password: "123", token: nanoid()}
  );

  const posts = await Post.create(
    {user: users[0]._id, title: 'Game of thrones', description: 'Serial super good', image: '', datetime: new Date().toISOString()},
    {user: users[1]._id, title: 'Football', image: '', datetime: new Date().toISOString()}
  );

  await Comment.create(
    {user: users[0]._id, post: posts[0]._id, description: 'good serial', datetime: new Date().toISOString()},
    {user: users[1]._id, post: posts[0]._id, description: 'good', datetime: new Date().toISOString()},
    {user: users[1]._id, post: posts[1]._id, description: 'football is awesome', datetime: new Date().toISOString()}
  );

  return connection.close();
};


run().catch(error => {
  console.error('Something wrong happened...', error);
});
